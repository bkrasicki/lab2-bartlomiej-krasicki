using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace lab1_bartlomiej_krasicki
{
    public class DatabaseContext
    {
        private string connectionString { get; set; }
        private const string Query = "SELECT * FROM PeopleLab";
        private const string QueryGetPersonById = "SELECT * FROM PeopleLab WHERE PersonId = ";

        public DatabaseContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public IEnumerable<Person> GetPeople()
        {
            var people = new List<Person>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(Query, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    people.Add(new Person
                    {
                        PersonId = Convert.ToInt32(reader["PersonId"]),
                        FirstName = reader["FirstName"].ToString(),
                        LastName = reader["LastName"].ToString(),
                        PhoneNumber = reader["PhoneNumber"].ToString()
                    });
                }
                reader.Close();
            }
            return people;
        }

        public void AddPerson([FromBody] Person person)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {   
                string queryAddPerson = $"INSERT INTO PeopleLab (FirstName, LastName, PhoneNumber) VALUES ('{person.FirstName}','{person.LastName}','{person.PhoneNumber}');";
                SqlCommand command = new SqlCommand(queryAddPerson, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                reader.Close();
            }
        }

        public void DeletePerson(int id)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string queryAddPerson = $"DELETE FROM PeopleLab WHERE PersonId = {id};";
                SqlCommand command = new SqlCommand(queryAddPerson, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                reader.Close();
            }
        }

        public Person GetPerson(int id)
        {
            Person person = null;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(QueryGetPersonById + "" + id, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    person = new Person
                    {
                        PersonId = Convert.ToInt32(reader["PersonId"]),
                        FirstName = reader["FirstName"].ToString(),
                        LastName = reader["LastName"].ToString(),
                        PhoneNumber = reader["PhoneNumber"].ToString()
                    };
                }
                reader.Close();
            }
            return person;
        }

        public Person UpdatePerson([FromBody] Person person, int id)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string queryAddPerson = $"UPDATE PeopleLab SET FirstName = '{person.FirstName}', LastName = '{person.LastName}', PhoneNumber = '{person.PhoneNumber}' WHERE PersonId = {id};";
                SqlCommand command = new SqlCommand(queryAddPerson, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    person = new Person
                    {
                        PersonId = Convert.ToInt32(reader["PersonId"]),
                        FirstName = reader["FirstName"].ToString(),
                        LastName = reader["LastName"].ToString(),
                        PhoneNumber = reader["PhoneNumber"].ToString()
                    };
                }
                reader.Close();
            }
            return person;
        }
    }
}