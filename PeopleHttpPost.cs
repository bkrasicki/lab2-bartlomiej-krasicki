using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using lab1_bartlomiej_krasicki;

namespace bartek_krasicki
{
    public static class PeopleHttpPost
    {

        //public static string cs = "Server=tcp:bkrasicki-cdv.database.windows.net,1433;Initial Catalog=bkrasicki_cdv;Persist Security Info=False;User ID=brasicki;Password=Bkr4sicki!;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

        [FunctionName("PeopleHttpPost")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = "person")] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");
            try
            {
                string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
                Person person = JsonConvert.DeserializeObject<Person>(requestBody);
                string connectionString = Environment.GetEnvironmentVariable("PeopleLab");
                log.LogInformation(connectionString);
                var db = new DatabaseContext(connectionString);
                db.AddPerson(person);
                return new OkObjectResult(person);
            }
            catch (Exception ex)
            {
                log.LogError(ex, ex.Message);
                return new JsonResult(ex);
            }
        }
    }
}
