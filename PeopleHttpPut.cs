using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using lab1_bartlomiej_krasicki;

namespace bartek_krasicki
{
    public static class PeopleHttpPut
    {
        [FunctionName("PeopleHttpPut")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "put", Route = "person/{id}")] HttpRequest req,
            ILogger log, int id)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");
            try
            {
                string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
                Person person = JsonConvert.DeserializeObject<Person>(requestBody);
                string connectionString = Environment.GetEnvironmentVariable("PeopleLab");
                log.LogInformation(connectionString);
                var db = new DatabaseContext(connectionString);
                var p = db.UpdatePerson(person, id);
                return new OkObjectResult(p);
            }
            catch (Exception ex)
            {
                log.LogError(ex, ex.Message);
                return new JsonResult(ex);
            }
        }
    }
}
